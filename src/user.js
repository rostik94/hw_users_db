import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';
import './styles/main.scss';

import { Users } from './js/model/usersModel';
import { selectOneUser } from './js/controllers/userController';
import { getUsersFromDB } from './js/controllers/userController';
import { renderSelectedUser } from './js/userFunctions/renderSelectedUser';

const contentField = document.getElementById('users-list');
const userID = localStorage.getItem('userID');

if (window.location.pathname === '/user.html') {
  (async () => {
    const user = await selectOneUser(userID);
    const data = await getUsersFromDB();
    const users = new Users();

    renderSelectedUser(contentField, user);

    users.listenersOnSelectedUser(user.data.user, userID);
    users.listenersOnNewUser();
    users.listenersOnNewUserPopUp();
    users.listenersOnSearchForm(contentField, data, user);
    users.listenersOnEditingPopUp();
  })();
}
