import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';
import './styles/main.scss';

import { getUsersFromDB } from './js/controllers/userController';
import { renderDB } from './js/userFunctions/renderDB';
import { Users } from './js/model/usersModel';

const contentField = document.getElementById('users-list');

if (window.location.pathname === '/') {
  (async () => {
    const data = await getUsersFromDB();
    const users = new Users();

    renderDB(contentField, data.data.users);

    users.listenersOnMainPage(data);
    users.listenersOnEditingPopUp();
    users.listenersOnNewUser();
    users.listenersOnNewUserPopUp();
    users.listenersOnSearchForm(contentField, data);
  })();
}
