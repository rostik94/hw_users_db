import { renderNewUserForm } from '../userFunctions/renderNewUserForm';
import { renderEditForm } from '../userFunctions/renderEditForm';
import { getUserValues } from '../userFunctions/getUserValues';
import { closePopUp } from '../userFunctions/closePopUp';
import { deleteSelectedUser } from '../userFunctions/deleteSelectedUser';
import { openSelectedUser } from '../userFunctions/openSelectedUser';
import { updateUser } from '../userFunctions/updateUser';
import { createUser } from '../userFunctions/createUser';
import { renderUserOnSearch } from '../userFunctions/renderUserOnSearch';
import { renderSelectedUser } from '../userFunctions/renderSelectedUser';
import { renderDB } from '../userFunctions/renderDB';

export class Users {
  listenersOnSearchForm(usersField, users, user) {
    const searchUserInput = document.getElementById('search-user__name');
    const listenersOnMainPage = this.listenersOnMainPage;
    const listenersOnSelectedUser = this.listenersOnSelectedUser;

    searchUserInput.addEventListener('keyup', () => {
      const userNameToLowerCase = searchUserInput.value.toLowerCase();
      const userName = new RegExp(`^${userNameToLowerCase}`);

      if (window.location.pathname === '/') {
        if (userName == '/^/') {
          renderDB(usersField, users.data.users);
          listenersOnMainPage(users);
        } else {
          renderUserOnSearch(usersField, users.data.users, userName);
          listenersOnMainPage(users);
        }
      } else if (window.location.pathname === '/user.html') {
        if (userName == '/^/') {
          renderSelectedUser(usersField, user);
          listenersOnSelectedUser(user.data.user, user.data.user._id);
        } else {
          renderUserOnSearch(usersField, users.data.users, userName);
          listenersOnMainPage(users);
        }
      }
    });
  }

  listenersOnMainPage(usersData) {
    const usersArray = document.querySelectorAll('[data-users="users-array"]');
    const editUserField = document.getElementById('edit-user');

    usersArray.forEach((user) => {
      user.addEventListener('click', (e) => {
        const deleteBtn = document.querySelector(
          `[data-delete-id="${user.id}"]`
        );
        const editBtn = document.querySelector(`[data-edit-id="${user.id}"]`);

        if (e.target === deleteBtn) {
          e.stopPropagation();
          deleteSelectedUser(user);
        } else if (e.target === editBtn) {
          e.stopPropagation();
          editUserField.classList.add('edit-user_active');
          renderEditForm(editUserField, user.id, usersData.data.users);
        } else {
          openSelectedUser(user);
        }
      });
    });
  }

  listenersOnNewUser() {
    const newUserField = document.getElementById('new-user');
    const newUserBtn = document.getElementById('btn-new-user');

    newUserBtn.addEventListener('click', () => {
      newUserField.classList.add('new-user_active');

      renderNewUserForm(newUserField);
    });
  }

  listenersOnEditingPopUp() {
    const userField = document.getElementById('edit-user');

    userField.addEventListener('click', (e) => {
      const updateBtn = document.querySelector(`[data-btn="update"]`);
      const editTitle = document.getElementById('edit-user__title');
      const userID = updateBtn.dataset.userId;

      if (e.target.closest('#edit-user__close') || e.target === userField) {
        closePopUp(userField, 'edit-user_active');
      } else if (e.target === updateBtn) {
        const userData = getUserValues('edit-user');

        editTitle.textContent = '';
        editTitle.insertAdjacentHTML(
          'afterbegin',
          `${userData.name} ${userData.surname}`
        );

        updateUser(userID, userData, 'edit-user');
      }
    });
  }

  listenersOnNewUserPopUp() {
    const newUserField = document.getElementById('new-user');

    newUserField.addEventListener('click', (e) => {
      const addUserBtn = document.querySelector(`[data-btn="create"]`);

      if (e.target.closest('#new-user__close') || e.target === newUserField) {
        closePopUp(newUserField, 'new-user_active');
      } else if (e.target === addUserBtn) {
        const userData = getUserValues('new-user');
        createUser(userData);
      }
    });
  }

  listenersOnSelectedUser(user, userID) {
    const deleteUserBtn = document.querySelector(`[data-btn="delete"]`);
    const updateUserBtn = document.querySelector(`[data-btn="update"]`);
    const selectedUserField = document.getElementById('users-list');
    const selectedUserTitle = document.getElementById('selected-user__title');

    selectedUserField.addEventListener('click', (e) => {
      if (e.target === deleteUserBtn) {
        deleteSelectedUser(user);
      } else if (e.target === updateUserBtn) {
        const userData = getUserValues('selected-user');

        updateUser(userID, userData, 'selected-user');

        if (userData.name && userData.surname) {
          selectedUserTitle.textContent = '';
          selectedUserTitle.insertAdjacentHTML(
            'afterbegin',
            `${userData.name} ${userData.surname}`
          );
        }
      }
    });
  }
}
