export const usersTemplate = (user) => {
  return `<li id="${user._id}" class="users-list__item" data-users="users-array">
  <div class="user-list__content">
    <div class="users-list__info">
      <span>Name:</span>
      <span>${user.name}</span>
    </div>
    <div class="users-list__info">
      <span>Surname:</span>
      <span>${user.surname}</span>
    </div>
  </div>
  <div class="users-list__buttons">
    <button class="btn btn_delete btn_small" type="button" data-delete-id="${user._id}">
      Delete
    </button>
    <button class="btn btn_edit btn_small" type="button" data-edit-id="${user._id}">Edit</button>
  </div>
</li>`;
};
