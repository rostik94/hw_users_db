export const newUserTemplate = `<div class="new-user__block">
<div id="new-user__close" class="new-user__close">
        <i class="fas fa-times"></i>
</div>
<h3 class="new-user__title">Create new user</h3>
<ul class="new-user__list">
  <li class="new-user__item">
    <label for="new-user__name" class="new-user__label"
      >Name:</label
    >
    <input
      type="text"
      class="new-user__input"
      id="new-user__name"
      required
    />
  </li>
  <li class="new-user__item">
    <label for="new-user__surname" class="new-user__label"
      >Surname:</label
    >
    <input
      type="text"
      class="new-user__input"
      id="new-user__surname"
      required
    />
  </li>
  <li class="new-user__item">
    <label for="new-user__age" class="new-user__label"
      >Age:</label
    >
    <input
      type="number"
      class="new-user__input"
      id="new-user__age"
      required
    />
  </li>
  <li class="new-user__item">
    <label for="new-user__phone-number" class="new-user__label"
      >Phone number:</label
    >
    <input
      type="text"
      class="new-user__input"
      id="new-user__phone-number"
      required
    />
  </li>
  <li class="new-user__item">
    <label for="new-user__email" class="new-user__label"
      >Email:</label
    >
    <input
      type="email"
      class="new-user__input"
      id="new-user__email"
      required
    />
  </li>
  <li class="new-user__item">
    <label for="new-user__city" class="new-user__label"
      >City:</label
    >
    <input
      type="text"
      class="new-user__input"
      id="new-user__city"
      required
    />
  </li>
  <li class="new-user__item">
    <label for="new-user__country" class="new-user__label"
      >Country:</label
    >
    <input
      type="text"
      class="new-user__input"
      id="new-user__country"
      required
    />
  </li>
  <li class="new-user__item">
    <label class="new-user__label">Married:</label>
    <div class="new-user__label-married">
      <label for="new-user__married-true">
        Yes
        <input
        type="radio"
        id="new-user__married-true"
        name="married"
        value = "true"
        checked="checked"
      />
      </label>
      
      <label for="new-user__married-false">
        No
        <input
        type="radio"
        id="new-user__married-false"
        name="married"
        value="false"
      />
      </label>
      
    </div>
  </li>
  <li class="new-user__item">
    <label for="new-user__company" class="new-user__label"
      >Company:</label
    >
    <input
      type="text"
      class="new-user__input"
      id="new-user__company"
    />
  </li>
  <li class="new-user__item">
    <label for="new-user__position" class="new-user__label"
      >Position:</label
    >
    <input
      type="text"
      class="new-user__input"
      id="new-user__position"
    />
  </li>
</ul>
<div class="new-user__buttons">
  <button class="btn btn_create btn_big" type="button" data-btn="create">Create</button>
</div>
<p class="new-user__status" id="new-user__status"></p>

</div>`;
