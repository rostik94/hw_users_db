export const editUserTemplate = (user) => {
  return `<div class="edit-user__block">
    <div id="edit-user__close" class="edit-user__close">
        <i class="fas fa-times"></i>
    </div>
    <h3 id="edit-user__title" class="edit-user__title">${user.name} ${
    user.surname
  }</h3>
        <ul class="edit-user__list">
            <li class="edit-user__item">
                <label for="edit-user__name" class="edit-user__label"
                >Name:</label
                >
                <input
                type="text"
                class="edit-user__input"
                id="edit-user__name"
                required
                value="${user.name}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__surname" class="edit-user__label"
                >Surname:</label
                >
                <input
                type="text"
                class="edit-user__input"
                id="edit-user__surname"
                required
                value="${user.surname}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__age" class="edit-user__label"
                >Age:</label
                >
                <input
                type="number"
                class="edit-user__input"
                id="edit-user__age"
                required
                value="${user.age}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__phone-number" class="edit-user__label"
                >Phone number:</label
                >
                <input
                type="text"
                class="edit-user__input"
                id="edit-user__phone-number"
                required
                value="${user.phoneNumber}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__email" class="edit-user__label"
                >Email:</label
                >
                <input
                type="email"
                class="edit-user__input"
                id="edit-user__email"
                required
                value="${user.email}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__city" class="edit-user__label"
                >City:</label
                >
                <input
                type="text"
                class="edit-user__input"
                id="edit-user__city"
                required
                value="${user.city}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__country" class="edit-user__label"
                >Country:</label
                >
                <input
                type="text"
                class="edit-user__input"
                id="edit-user__country"
                required
                value="${user.country}"
                />
            </li>
            <li class="edit-user__item">
                <label class="edit-user__label">Married:</label>
                <div class="edit-user__input-married">
                <label for="edit-user__married-true">
                    Yes
                    <input type="radio" id="edit-user__married-true" name="married"
                    value = "true" ${user.married ? 'checked="true"' : false} />
                </label>
                
                <label for="edit-user__married-false">
                    No
                    <input type="radio" id="edit-user__married-false" name="married"
                    value="false" ${!user.married ? 'checked="true"' : false} />
                </label>
                
                </div>
            </li>
            <li class="edit-user__item">
                <label for="edit-user__company" class="edit-user__label"
                >Company:</label
                >
                <input
                type="text"
                class="edit-user__input"
                id="edit-user__company"
                value="${user.company}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__position" class="edit-user__label"
                >Position:</label
                >
                <input
                type="text"
                class="edit-user__input"
                id="edit-user__position"
                value="${user.position}"
                />
            </li>
            <li class="edit-user__item">
                <label for="edit-user__created-at" class="edit-user__label"
                >Created at:</label
                >
                <span>${user.createdAt}</span>
            </li>
        </ul>
        <div class="edit-user__buttons">
        <button class="btn btn_update btn_big" type="button" data-user-id="${
          user._id
        }" data-btn="update">
              Update
          </button>
        </div>
        <p class="edit-user__status" id="edit-user__status"></p>
    </div>`;
};
