export const selectedUserTemplate = (user) => {
  return `<div id="${user._id}" class="selected-user">
  <h3 id="selected-user__title" class="selected-user__title">
    ${user.name} 
    ${user.surname}</h3>
  <ul class="selected-user__list">
    <li class="selected-user__item">
      <label for="selected-user__name" class="selected-user__label"
        >Name:</label
      >
      <input
        type="text"
        class="selected-user__input"
        id="selected-user__name"
        required
        value="${user.name}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__surname" class="selected-user__label"
        >Surname:</label
      >
      <input
        type="text"
        class="selected-user__input"
        id="selected-user__surname"
        required
        value="${user.surname}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__age" class="selected-user__label"
        >Age:</label
      >
      <input
        type="number"
        class="selected-user__input"
        id="selected-user__age"
        required
        value="${user.age}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__phone-number" class="selected-user__label"
        >Phone number:</label
      >
      <input
        type="text"
        class="selected-user__input"
        id="selected-user__phone-number"
        required
        value="${user.phoneNumber}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__email" class="selected-user__label"
        >Email:</label
      >
      <input
        type="email"
        class="selected-user__input"
        id="selected-user__email"
        required
        value="${user.email}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__city" class="selected-user__label"
        >City:</label
      >
      <input
        type="text"
        class="selected-user__input"
        id="selected-user__city"
        required
        value="${user.city}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__country" class="selected-user__label"
        >Country:</label
      >
      <input
        type="text"
        class="selected-user__input"
        id="selected-user__country"
        required
        value="${user.country}"
      />
    </li>
    <li class="selected-user__item">
      <label class="selected-user__label">Married:</label>
      <div class="selected-user__label-married">
        <label for="selected-user__married-true">Yes</label>
        <input
          type="radio"
          id="selected-user__married-true"
          name="married"
          value = "true"
          ${user.married ? 'checked="true"' : false}
          
        />
        <label for="selected-user__married-false">No</label>
        <input
          type="radio"
          id="selected-user__married-false"
          name="married"
          value="false"
          ${!user.married ? 'checked="true"' : false}
        />
      </div>
    </li>
    <li class="selected-user__item">
      <label for="selected-user__company" class="selected-user__label"
        >Company:</label
      >
      <input
        type="text"
        class="selected-user__input"
        id="selected-user__company"
        value="${user.company}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__position" class="selected-user__label"
        >Position:</label
      >
      <input
        type="text"
        class="selected-user__input"
        id="selected-user__position"
        value="${user.position}"
      />
    </li>
    <li class="selected-user__item">
      <label for="selected-user__created-at" class="selected-user__label"
        >Created at:</label
      >
      <span>${user.createdAt}</span>
    </li>
  </ul>
  <div class="selected-user__buttons">
    <button class="btn btn_delete btn_big" type="button" data-btn="delete">
      Delete
    </button>
    <button class="btn btn_update btn_big" type="button" data-btn="update">
      Update
    </button>
  </div>
  <p id="selected-user__status" class="selected-user__status"></p>
  </div>`;
};
