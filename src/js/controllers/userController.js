const url = `http://localhost:8000/api/v1/users`;

export const getUsersFromDB = async () => {
  const users = await fetch(url);
  return await users.json();
};

export const deleteUserFromDB = async (userID) => {
  await fetch(`${url}/${userID}`, {
    method: 'DELETE',
  });
};

export const searchUserByName = async (name) => {
  const user = await fetch(`${url}?name=${name}`);
  return await user.json();
};

export const selectOneUser = async (userID) => {
  const user = await fetch(`${url}/${userID}`);
  return await user.json();
};

export const createNewUser = async (data) => {
  const user = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  });

  return await user.json();
};

export const updateSelectedUser = async (userID, data) => {
  const user = await fetch(`${url}/${userID}`, {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  });

  return await user.json();
};
