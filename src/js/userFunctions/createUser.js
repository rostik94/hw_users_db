import { createNewUser } from '../controllers/userController';

export const createUser = async (userData) => {
  const status = document.getElementById('new-user__status');

  try {
    const result = await createNewUser(userData);

    if (result.status === 'success') {
      status.textContent = '';
      status.style.color = 'green';
      status.insertAdjacentHTML('afterbegin', 'User successfully updated');
    } else throw result.message;
  } catch (err) {
    status.textContent = '';
    status.style.color = 'red';
    status.insertAdjacentHTML('afterbegin', err);
  }
};
