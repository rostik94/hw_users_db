import { deleteUserFromDB } from '../controllers/userController';

export const deleteSelectedUser = async (user) => {
  await deleteUserFromDB(user.id || user._id);

  if (window.location.pathname === '/') {
    user.classList.add('users-list__item_delete');
  } else {
    document.location.href = '/';
  }
};
