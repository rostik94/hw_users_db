import { usersTemplate } from '../templates/usersTemplate';

export const renderDB = (contentField, users) => {
  contentField.textContent = '';

  users.map((user) => {
    contentField.insertAdjacentHTML('afterbegin', usersTemplate(user));
  });
};
