import { updateSelectedUser } from '../controllers/userController';

export const updateUser = async (userID, userData, statusID) => {
  const status = document.getElementById(`${statusID}__status`);

  try {
    const result = await updateSelectedUser(userID, userData);

    if (result.status === 'success') {
      status.textContent = '';
      status.style.color = 'green';
      status.insertAdjacentHTML('afterbegin', 'User successfully updated');
    } else throw result.message;
  } catch (err) {
    status.textContent = '';
    status.style.color = 'red';
    status.insertAdjacentHTML('afterbegin', err);
  }
};
