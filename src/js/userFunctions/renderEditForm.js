import { editUserTemplate } from '../templates/editUserTemplate';

export const renderEditForm = (contentField, userID, usersArray) => {
  const userData = usersArray.find((user) => user._id === userID);

  contentField.textContent = '';
  contentField.insertAdjacentHTML('afterbegin', editUserTemplate(userData));
};
