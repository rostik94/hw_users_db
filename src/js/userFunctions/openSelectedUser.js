export const openSelectedUser = (user) => {
  const globalUserID = user.id;
  const newUrl = '/user.html';

  localStorage.setItem('userID', globalUserID);
  document.location.href = newUrl;
};
