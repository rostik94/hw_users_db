import { newUserTemplate } from '../templates/newUserTemplate';

export const renderNewUserForm = (contentField) => {
  contentField.textContent = '';
  contentField.insertAdjacentHTML('afterbegin', newUserTemplate);
};
