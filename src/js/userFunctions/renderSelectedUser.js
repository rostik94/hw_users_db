import { selectedUserTemplate } from '../templates/selectedUserTemplate';

export const renderSelectedUser = (contentField, userData) => {
  contentField.textContent = '';

  if (userData) {
    contentField.insertAdjacentHTML(
      'afterbegin',
      selectedUserTemplate(userData.data.user)
    );
  }
};
