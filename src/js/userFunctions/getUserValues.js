export const getUserValues = (className) => {
  return {
    name: document.querySelector(`#${className}__name`).value,
    surname: document.querySelector(`#${className}__surname`).value,
    age: document.querySelector(`#${className}__age`).value,
    phoneNumber: document.querySelector(`#${className}__phone-number`).value,
    email: document.querySelector(`#${className}__email`).value,
    city: document.querySelector(`#${className}__city`).value,
    country: document.querySelector(`#${className}__country`).value,
    married: document.querySelector(`input[name="married"]:checked`).value,
    company: document.querySelector(`#${className}__company`).value,
    position: document.querySelector(`#${className}__position`).value,
  };
};
