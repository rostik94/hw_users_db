export const closePopUp = (editUserField, classActive) => {
  editUserField.classList.remove(classActive);
  editUserField.textContent = '';
};
