import { usersTemplate } from '../templates/usersTemplate';

export const renderUserOnSearch = (contentField, usersData, userName) => {
  const findUsers = usersData.filter((user) =>
    userName.test(user.name.toLowerCase())
  );

  contentField.textContent = '';

  findUsers.forEach((user) => {
    contentField.insertAdjacentHTML('afterbegin', usersTemplate(user));
  });
};
